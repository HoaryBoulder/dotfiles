#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run redshift -l 47.41667:86.6667
run picom
run nitrogen --restore
